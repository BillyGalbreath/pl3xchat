package net.pl3x.bukkit.chat.hook;

import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultHook {
    private Permission permission = null;
    private Chat chat = null;

    public boolean failedToSetupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = Bukkit.getServicesManager().getRegistration(Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return permission == null;
    }

    public boolean failedToSetupChat() {
        RegisteredServiceProvider<Chat> chatProvider = Bukkit.getServicesManager().getRegistration(Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }
        return chat == null;
    }

    public String getPrefix(Player player) {
        World world = player.getWorld();
        String[] groups = permission.getPlayerGroups(player);
        StringBuilder prefixes = new StringBuilder();
        for (String groupName : groups) {
            prefixes.append(chat.getGroupPrefix(world, groupName));
        }
        return prefixes.toString();
    }

    public String getSuffix(Player player) {
        World world = player.getWorld();
        String[] groups = permission.getPlayerGroups(player);
        StringBuilder suffixes = new StringBuilder();
        for (String groupName : groups) {
            suffixes.append(chat.getGroupSuffix(world, groupName));
        }
        return suffixes.toString();
    }

    public String getPrimaryGroup(Player player) {
        return permission.getPrimaryGroup(player);
    }
}
