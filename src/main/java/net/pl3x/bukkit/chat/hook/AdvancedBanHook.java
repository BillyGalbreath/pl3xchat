package net.pl3x.bukkit.chat.hook;

import me.leoko.advancedban.manager.PunishmentManager;
import org.bukkit.entity.Player;

public class AdvancedBanHook {
    public boolean isMuted(Player player) {
        return PunishmentManager.get().isMuted(player.getUniqueId().toString());
    }
}
