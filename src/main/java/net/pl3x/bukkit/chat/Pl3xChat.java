package net.pl3x.bukkit.chat;

import net.pl3x.bukkit.chat.commands.CmdFlip;
import net.pl3x.bukkit.chat.commands.CmdFlipText;
import net.pl3x.bukkit.chat.commands.CmdIgnore;
import net.pl3x.bukkit.chat.commands.CmdMe;
import net.pl3x.bukkit.chat.commands.CmdMute;
import net.pl3x.bukkit.chat.commands.CmdPl3xChat;
import net.pl3x.bukkit.chat.commands.CmdReply;
import net.pl3x.bukkit.chat.commands.CmdRussia;
import net.pl3x.bukkit.chat.commands.CmdSay;
import net.pl3x.bukkit.chat.commands.CmdShrug;
import net.pl3x.bukkit.chat.commands.CmdSpy;
import net.pl3x.bukkit.chat.commands.CmdStaff;
import net.pl3x.bukkit.chat.commands.CmdTell;
import net.pl3x.bukkit.chat.commands.CmdUnflip;
import net.pl3x.bukkit.chat.configuration.Config;
import net.pl3x.bukkit.chat.configuration.Lang;
import net.pl3x.bukkit.chat.configuration.PlayerConfig;
import net.pl3x.bukkit.chat.hook.AdvancedBanHook;
import net.pl3x.bukkit.chat.hook.DiscordSRVHook;
import net.pl3x.bukkit.chat.hook.VaultHook;
import net.pl3x.bukkit.chat.listeners.AdvancedBanListener;
import net.pl3x.bukkit.chat.listeners.BukkitListener;
import net.pl3x.bukkit.chat.listeners.ChatListener;
import net.pl3x.bukkit.chat.listeners.DiscordSRVListener;
import net.pl3x.bukkit.chat.listeners.SuperVanishListener;
import net.pl3x.bukkit.chat.listeners.VoteListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xChat extends JavaPlugin {
    private final Logger logger;
    private DiscordSRVHook discordSRVHook;
    private AdvancedBanHook advancedBanHook;
    private VaultHook vaultHook;

    public Pl3xChat() {
        this.logger = new Logger(this);
    }

    public Logger getLog() {
        return logger;
    }

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        if (getServer().getPluginManager().isPluginEnabled("Vault")) {
            vaultHook = new VaultHook();
            if (vaultHook.failedToSetupPermissions() || vaultHook.failedToSetupChat()) {
                getLog().warn("Vault dependency was found, but no permissions hook!");
                getLog().warn("A permissions plugin is required to be installed!");
                getLog().warn("Prefixes and suffixes will not be available in chat");
                vaultHook = null;
            }
        } else {
            getLog().warn("Vault dependency not found!");
            getLog().warn("Prefixes and suffixes will not be available in chat");
        }

        if (getServer().getPluginManager().isPluginEnabled("DiscordSRV")) {
            discordSRVHook = new DiscordSRVHook();
            Bukkit.getPluginManager().registerEvents(new DiscordSRVListener(this), this);

            if (getServer().getPluginManager().isPluginEnabled("SuperbVote")) {
                Bukkit.getPluginManager().registerEvents(new VoteListener(this), this);
            }

            if (getServer().getPluginManager().isPluginEnabled("SuperVanish")) {
                Bukkit.getPluginManager().registerEvents(new SuperVanishListener(this), this);
            }
        }

        if (getServer().getPluginManager().isPluginEnabled("AdvancedBan")) {
            advancedBanHook = new AdvancedBanHook();
            Bukkit.getPluginManager().registerEvents(new AdvancedBanListener(this), this);
        }

        Bukkit.getPluginManager().registerEvents(new ChatListener(this), this);
        Bukkit.getPluginManager().registerEvents(new BukkitListener(this), this);

        getCommand("flip").setExecutor(new CmdFlip());
        getCommand("fliptext").setExecutor(new CmdFlipText());
        getCommand("ignore").setExecutor(new CmdIgnore(this));
        getCommand("pl3xchat").setExecutor(new CmdPl3xChat(this));
        getCommand("me").setExecutor(new CmdMe(this));
        getCommand("mute").setExecutor(new CmdMute(this));
        getCommand("reply").setExecutor(new CmdReply(this));
        getCommand("russia").setExecutor(new CmdRussia());
        getCommand("say").setExecutor(new CmdSay(this));
        getCommand("shrug").setExecutor(new CmdShrug());
        getCommand("spy").setExecutor(new CmdSpy(this));
        getCommand("staff").setExecutor(new CmdStaff(this));
        getCommand("tell").setExecutor(new CmdTell(this));
        getCommand("unflip").setExecutor(new CmdUnflip());

        getLog().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        PlayerConfig.removeAll();

        getLog().info(getName() + " disabled.");
    }

    public DiscordSRVHook getDiscordSRVHook() {
        return discordSRVHook;
    }

    public AdvancedBanHook getAdvancedBanHook() {
        return advancedBanHook;
    }

    public VaultHook getVaultHook() {
        return vaultHook;
    }
}
