package net.pl3x.bukkit.chat.listeners;

import io.minimum.minecraft.superbvote.votes.SuperbVoteEvent;
import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Lang;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class VoteListener implements Listener {
    private final Pl3xChat plugin;

    public VoteListener(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onVoteReceived(SuperbVoteEvent event) {
        if (plugin.getDiscordSRVHook() == null) {
            return; // no bot
        }

        if (event.getVoteRewards().isEmpty()) {
            return; // no rewards given
        }

        plugin.getDiscordSRVHook().sendToDiscord(Lang.VOTE_RECEIVED
                .replace("{player}", event.getVote().getName())
                .replace("{service}", event.getVote().getServiceName()));
    }
}
