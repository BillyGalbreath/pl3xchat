package net.pl3x.bukkit.chat.listeners;

import me.leoko.advancedban.event.PunishmentEvent;
import me.leoko.advancedban.event.RevokePunishmentEvent;
import me.leoko.advancedban.utils.Punishment;
import net.pl3x.bukkit.chat.Pl3xChat;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class AdvancedBanListener implements Listener {
    private final Pl3xChat plugin;

    public AdvancedBanListener(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPunishmentCreated(PunishmentEvent event) {
        if (plugin.getDiscordSRVHook() == null) {
            return;
        }

        Punishment punishment = event.getPunishment();
        if (punishment == null) {
            return;
        }

        String reason = punishment.getReason();
        reason = reason == null || reason.isEmpty() || reason.equals("none")
                ? "" : "```" + reason + "```";

        String duration = punishment.getDuration(true);
        duration = duration == null || duration.isEmpty() || duration.equals("permanent")
                ? "" : " (for " + duration + ")";

        String action = "punished";
        String icon = ":warning:";
        switch (punishment.getType()) {
            case BAN:
            case TEMP_BAN:
            case IP_BAN:
                action = "banned";
                icon = ":ban_hammer:";
                break;
            case MUTE:
            case TEMP_MUTE:
                action = "muted";
                icon = ":zipper_mouth:";
                break;
            case WARNING:
            case TEMP_WARNING:
                action = "warned";
                icon = ":passport_control:";
                break;
            case KICK:
                action = "kicked";
                icon = ":middle_finger:";
                break;
        }

        plugin.getDiscordSRVHook().sendToDiscord(icon + " **" + punishment.getName() + " was "
                + action + " by " + punishment.getOperator() + "**" + duration + reason);
    }

    @EventHandler
    public void onPunishmentRevoked(RevokePunishmentEvent event) {
        if (plugin.getDiscordSRVHook() == null) {
            return;
        }

        Punishment punishment = event.getPunishment();
        if (punishment == null) {
            return;
        }

        String action = "punishment";
        switch (punishment.getType()) {
            case BAN:
            case TEMP_BAN:
            case IP_BAN:
                action = "ban";
                break;
            case MUTE:
            case TEMP_MUTE:
                action = "mute";
                break;
            case WARNING:
            case TEMP_WARNING:
                action = "warn";
                break;
            case KICK:
                return;
        }

        plugin.getDiscordSRVHook().sendToDiscord(":metal: **" + punishment.getName()
                + "'s " + action + " has been lifted**");
    }
}
