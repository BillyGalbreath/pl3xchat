package net.pl3x.bukkit.chat.listeners;

import net.minecraft.server.v1_12_R1.Advancement;
import net.minecraft.server.v1_12_R1.AdvancementDisplay;
import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Lang;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_12_R1.advancement.CraftAdvancement;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class DiscordSRVListener implements Listener {
    private final Pl3xChat plugin;

    public DiscordSRVListener(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        plugin.getDiscordSRVHook().sendToDiscord(
                (event.getPlayer().hasPlayedBefore() ? Lang.JOIN_GAME_DISCORD : Lang.FIRST_JOIN_GAME_DISCORD)
                        .replace("{join-message}", event.getJoinMessage()));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        plugin.getDiscordSRVHook().sendToDiscord(Lang.QUIT_GAME_DISCORD
                .replace("{quit-message}", event.getQuitMessage()));
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerAdvancement(PlayerAdvancementDoneEvent event) {
        if (plugin.getDiscordSRVHook() == null) {
            return;
        }

        if (!event.getPlayer().getWorld().isGameRule("announceAdvancements")) {
            return; // advancements not enabled in this world per gamerule
        }

        Advancement nmsAdvancement = ((CraftAdvancement) event.getAdvancement()).getHandle();
        if (nmsAdvancement == null) {
            return;
        }
        AdvancementDisplay nmsDisplay = nmsAdvancement.c();
        if (nmsDisplay == null || !nmsDisplay.i()) {
            return; // not announced to chat, ignore
        }

        NamespacedKey key = event.getAdvancement().getKey();
        String path = ("advancements." + key.getNamespace() + "." + key.getKey())
                .replace("/", ".");
        String title = plugin.getConfig().getString(path, path);

        plugin.getDiscordSRVHook().sendToDiscord(Lang.ADVANCEMENT_DISCORD
                .replace("{player}", event.getPlayer().getName())
                .replace("{title}", title));
    }
}
