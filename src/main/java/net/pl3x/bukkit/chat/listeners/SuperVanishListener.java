package net.pl3x.bukkit.chat.listeners;

import de.myzelyam.api.vanish.PlayerHideEvent;
import de.myzelyam.api.vanish.PlayerShowEvent;
import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Lang;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class SuperVanishListener implements Listener {
    private final Pl3xChat plugin;

    public SuperVanishListener(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerUnvanish(PlayerShowEvent event) {
        if (plugin.getDiscordSRVHook() == null) {
            return; // no bot
        }

        plugin.getDiscordSRVHook().sendToDiscord(Lang.JOIN_GAME_DISCORD
                .replace("{join-message}", event.getPlayer().getName() + " joined the game"));
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerVanish(PlayerHideEvent event) {
        if (plugin.getDiscordSRVHook() == null) {
            return; // no bot
        }

        plugin.getDiscordSRVHook().sendToDiscord(Lang.QUIT_GAME_DISCORD
                .replace("{quit-message}", event.getPlayer().getName() + " left the game"));
    }
}
