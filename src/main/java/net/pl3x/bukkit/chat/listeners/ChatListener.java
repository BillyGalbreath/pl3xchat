package net.pl3x.bukkit.chat.listeners;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Config;
import net.pl3x.bukkit.chat.configuration.Lang;
import net.pl3x.bukkit.chat.configuration.PlayerConfig;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class ChatListener implements Listener {
    private final Pl3xChat plugin;
    private Class<?> nbtTagCompound;
    private Method nmsCopy;

    public ChatListener(Pl3xChat plugin) {
        this.plugin = plugin;
        String version = Bukkit.getServer().getClass().toString().split("\\.")[3];
        try {
            nbtTagCompound = Class.forName("net.minecraft.server." + version + ".NBTTagCompound");
            nmsCopy = Class.forName("org.bukkit.craftbukkit." + version + ".inventory.CraftItemStack")
                    .getDeclaredMethod("asNMSCopy", ItemStack.class);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            plugin.getLog().warn("Something went wrong detecting server version. Weapon names will not display correctly..");
            plugin.getLog().warn(e.getLocalizedMessage());
        }
    }

    /*
     * Try to stop mutes early
     */
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerChatEarly(AsyncPlayerChatEvent event) {
        if (PlayerConfig.getConfig(plugin, event.getPlayer()).isMuted()) {
            Lang.send(event.getPlayer(), Lang.YOU_ARE_MUTED);
            event.setCancelled(true);
        }

        // text replacements
        String message = event.getMessage();
        for (Map.Entry<String, String> entry : Config.TEXT_REPLACEMENTS.entrySet()) {
            message = message.replaceAll("(?i)" + Pattern.quote(entry.getKey()), entry.getValue());
        }
        event.setMessage(message);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        boolean isStaffChat = false;
        String message = event.getMessage();

        // check for staff chat
        Player sender = event.getPlayer();
        if (message.startsWith("s:")) {
            if (!sender.hasPermission("command.staff")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                event.setCancelled(true);
                return;
            }

            isStaffChat = true;
            message = message.substring(2);

            event.getRecipients().removeIf(recipient ->
                    !recipient.hasPermission("command.staff"));
        }

        ItemStack itemInHand = sender.getInventory().getItemInMainHand().clone();
        String itemName = "";
        String itemTag = "";
        try {
            // remove all page content from books (to prevent client crashes from string too big)
            if (itemInHand.getType() == Material.BOOK_AND_QUILL ||
                    itemInHand.getType() == Material.WRITTEN_BOOK) {
                BookMeta meta = (BookMeta) itemInHand.getItemMeta();
                meta.setPages(new ArrayList<>());
                itemInHand.setItemMeta(meta);
            }

            Object nms = nmsCopy.invoke(null, itemInHand);
            Object tag = nbtTagCompound.newInstance();
            nms.getClass().getDeclaredMethod("save", nbtTagCompound).invoke(nms, tag);

            itemName = (String) nms.getClass().getDeclaredMethod("getName").invoke(nms);
            if (itemInHand.getAmount() > 1) {
                itemName = Lang.ITEM_FORMAT_MULTI
                        .replace("{item}", itemName)
                        .replace("{amount}", Integer.toString(itemInHand.getAmount()));
            } else {
                itemName = Lang.ITEM_FORMAT.replace("{item}", itemName);
            }
            itemTag = tag.toString();
        } catch (IllegalAccessException | NoSuchMethodException | InstantiationException | InvocationTargetException ignore) {
        }

        // check for color codes
        message = sender.hasPermission("chat.color") ?
                message.replaceAll("(?i)&([a-f0-9r])", "\u00a7$1") :
                message.replaceAll("(?i)&([a-f0-9r])", "")
                        .replaceAll("(?i)\u00a7([a-f0-9r])", "");

        // check for format codes
        message = sender.hasPermission("chat.format") ?
                message.replaceAll("(?i)&([k-or])", "\u00a7$1") :
                message.replaceAll("(?i)&([k-or])", "")
                        .replaceAll("(?i)\u00a7([k-or])", "");

        // fix up the format
        String format = ChatColor.translateAlternateColorCodes('&',
                (isStaffChat ? Lang.STAFF_FORMAT : Lang.CHAT_FORMAT)
                        .replace("{prefix}", plugin.getVaultHook() == null ? "" :
                                plugin.getVaultHook().getPrefix(sender))
                        .replace("{suffix}", plugin.getVaultHook() == null ? "" :
                                plugin.getVaultHook().getSuffix(sender))
                        .replace("{message}", message));

        // finish it up
        String[] parts = format.split("\\{sender}");

        BaseComponent[] firstComponents = TextComponent.fromLegacyText(parts[0]);
        BaseComponent[] secondComponents;

        if (parts[1].contains("[item]")) {
            String[] itemParts = parts[1].split("\\[item]");

            BaseComponent[] firstItemComponents = TextComponent.fromLegacyText(itemParts[0]);
            BaseComponent[] secondItemComponents = TextComponent.fromLegacyText(
                    String.join(" ", Arrays.copyOfRange(itemParts, 1, itemParts.length)));
            BaseComponent[] itemComponents = TextComponent.fromLegacyText(
                    ChatColor.translateAlternateColorCodes('&',
                            ChatColor.getLastColors(itemParts[0]) + itemName));
            BaseComponent[] itemHoverComponents = TextComponent.fromLegacyText(itemTag);
            for (BaseComponent itemComponent : itemComponents) {
                itemComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, itemHoverComponents));
            }

            List<BaseComponent> baseComponents = new ArrayList<>();
            Collections.addAll(baseComponents, firstItemComponents);
            Collections.addAll(baseComponents, itemComponents);
            Collections.addAll(baseComponents, secondItemComponents);
            secondComponents = baseComponents.toArray(new BaseComponent[0]);
        } else {
            secondComponents = TextComponent.fromLegacyText(
                    String.join(" ", Arrays.copyOfRange(parts, 1, parts.length)));
        }


        TextComponent senderComponents = new TextComponent(ChatColor.getLastColors(parts[0]) +
                sender.getDisplayName());
        senderComponents.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
                Lang.CLICK_SUGGEST_COMMAND
                        .replace("{name}", sender.getName())
                        .replace("{display-name}", sender.getDisplayName())));

        senderComponents.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&',
                        Lang.HOVER_TOOLTIP
                                .replace("{prefix}", plugin.getVaultHook() == null ? "" :
                                        plugin.getVaultHook().getPrefix(sender))
                                .replace("{group}", plugin.getVaultHook() == null ? "" :
                                        WordUtils.capitalizeFully(plugin.getVaultHook().getPrimaryGroup(sender)))
                                .replace("{name}", sender.getName())
                                .replace("{display-name}", sender.getDisplayName())))));

        List<BaseComponent> componentsList = new ArrayList<>();
        componentsList.addAll(Arrays.asList(firstComponents));
        componentsList.add(senderComponents);
        componentsList.addAll(Arrays.asList(secondComponents));

        BaseComponent[] components = new BaseComponent[componentsList.size()];
        componentsList.toArray(components);

        for (Player recipient : event.getRecipients()) {
            recipient.sendMessage(components);
        }

        if (isStaffChat) {
            // chat handled (stops discord bot and other plugins from receiving)
            event.setCancelled(true);

            // inform console for logging purposes
            plugin.getServer().getConsoleSender().sendMessage(format
                    .replace("{sender}", sender.getName())
                    .replace("{message}", message));

            return; // we're done
        }

        // clean it up for console
        if (message.contains("[item]")) {
            String item = "";

            ItemMeta meta = null;
            if (itemInHand.hasItemMeta()) {
                meta = itemInHand.getItemMeta();
            }

            Map<Enchantment, Integer> enchants = new HashMap<>(itemInHand.getEnchantments());
            if (itemInHand.getType() == Material.ENCHANTED_BOOK) {
                enchants.putAll(((EnchantmentStorageMeta) itemInHand.getItemMeta()).getStoredEnchants());
            }
            if (!enchants.isEmpty() && (meta == null || !meta.hasItemFlag(ItemFlag.HIDE_ENCHANTS))) {
                StringBuilder enchantTags = new StringBuilder();
                for (Map.Entry<Enchantment, Integer> entry : enchants.entrySet()) {
                    enchantTags.append("\n    ")
                            .append(getEnchantName(entry.getKey()))
                            .append(getRoman(entry.getValue()));
                }
                item = item + "\n  Enchantments:" + enchantTags;
            }

            if (meta != null && meta.hasLore()) {
                StringBuilder loreTags = new StringBuilder();
                for (String lore : meta.getLore()) {
                    loreTags.append("\n    ").append(lore);
                }
                item = item + "\n  Lore:" + loreTags;
            }

            message = "&r" + ChatColor.stripColor(message)
                    .replace("[item]", itemName + "&r") +
                    (item.isEmpty() ? "" : "```" + itemName
                            .replace("[", "")
                            .replace("]", "") +
                            "&r:" + item + "```");
        }

        event.getRecipients().clear();
        event.setMessage(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', message)));
    }

    private String getRoman(int num) {
        switch (num) {
            case 0:
            case 1:
                return "";
            case 2:
                return " II";
            case 3:
                return " III";
            case 4:
                return " IV";
            case 5:
                return " V";
            case 6:
                return " VI";
            case 7:
                return " VII";
            case 8:
                return " VIII";
            case 9:
                return " IX";
            case 10:
                return " X";
            default:
                return " " + Integer.toString(num);
        }
    }

    private String getEnchantName(Enchantment enchantment) {
        //noinspection deprecation
        switch (enchantment.getId()) {
            case 0:
                return "Protection";
            case 1:
                return "Fire Protection";
            case 2:
                return "Feather Falling";
            case 3:
                return "Blast Protection";
            case 4:
                return "Projectile Protection";
            case 5:
                return "Respiration";
            case 6:
                return "Aqua Affinity";
            case 7:
                return "Thorns";
            case 8:
                return "Depth Strider";
            case 9:
                return "Frost Walker";
            case 10:
                return "Curse of Binding";
            case 16:
                return "Sharpness";
            case 17:
                return "Smite";
            case 18:
                return "Bane of Arthropods";
            case 19:
                return "Knockback";
            case 20:
                return "Fire Aspect";
            case 21:
                return "Looting";
            case 22:
                return "Sweeping Edge";
            case 32:
                return "Efficiency";
            case 33:
                return "Silk Touch";
            case 34:
                return "Unbreaking";
            case 35:
                return "Fortune";
            case 48:
                return "Power";
            case 49:
                return "Punch";
            case 50:
                return "Flame";
            case 51:
                return "Infinity";
            case 61:
                return "Luck of the Sea";
            case 62:
                return "Lure";
            case 70:
                return "Mending";
            case 71:
                return "Curse of Vanishing";
            case 128:
                return "Lava Walker";
            default:
                return enchantment.getName();
        }
    }
}
