package net.pl3x.bukkit.chat.commands;

import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Lang;
import net.pl3x.bukkit.chat.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class CmdIgnore implements TabExecutor {
    private final Pl3xChat plugin;

    public CmdIgnore(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.ignore")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        Player player = (Player) sender;
        PlayerConfig senderConfig = PlayerConfig.getConfig(plugin, player);

        if (args.length == 0 || args[0].equalsIgnoreCase("list")) {
            StringBuilder sb = new StringBuilder();
            for (UUID uuid : senderConfig.getIgnoreList()) {
                OfflinePlayer ignored = Bukkit.getOfflinePlayer(uuid);
                if (ignored == null) {
                    continue;
                }
                if (!sb.toString().isEmpty()) {
                    sb.append(", ");
                }
                sb.append(ignored.getName());
            }
            String names = sb.toString();
            Lang.send(sender, Lang.IGNORE_LIST
                    .replace("{list}", names.isEmpty() ? "none" : names));
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            Lang.send(sender, Lang.PLAYER_NOT_FOUND);
            return true;
        }

        if (!senderConfig.isIgnoring(target) && target.hasPermission("exempt.ignore")) {
            Lang.send(sender, Lang.EXEMPT_IGNORE
                    .replace("{target}", target.getName()));
            return true;
        }

        Set<UUID> ignoreList = senderConfig.getIgnoreList();
        if (senderConfig.isIgnoring(target)) {
            ignoreList.remove(target.getUniqueId());
        } else {
            ignoreList.add(target.getUniqueId());
        }
        senderConfig.setIgnoreList(ignoreList);

        Lang.send(sender, (senderConfig.isIgnoring(target) ? Lang.TARGET_IGNORED : Lang.TARGET_UNIGNORED)
                .replace("{target}", target.getName()));
        return true;
    }
}
