package net.pl3x.bukkit.chat.commands;

import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Lang;
import net.pl3x.bukkit.chat.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CmdMute implements TabExecutor {
    private final Pl3xChat plugin;

    public CmdMute(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.mute")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (plugin.getAdvancedBanHook() != null) {
            Lang.send(sender, "&6AdvancedBan has taken over mute handling. Please use their command(s)");
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.MUST_SPECIFY_PLAYER);
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            Lang.send(sender, Lang.PLAYER_NOT_FOUND);
            return true;
        }

        PlayerConfig targetConfig = PlayerConfig.getConfig(plugin, target);
        if (!targetConfig.isMuted() && target.hasPermission("exempt.mute")) {
            Lang.send(sender, Lang.EXEMPT_MUTE
                    .replace("{target}", target.getName()));
            return true;
        }

        targetConfig.setMuted(!targetConfig.isMuted());

        Lang.send(sender, (targetConfig.isMuted() ? Lang.TARGET_MUTED : Lang.TARGET_UNMUTED)
                .replace("{target}", target.getName()));
        return true;
    }
}
