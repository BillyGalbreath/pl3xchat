package net.pl3x.bukkit.chat.commands;

import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Lang;
import net.pl3x.bukkit.chat.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class CmdMe implements TabExecutor {
    private final Pl3xChat plugin;

    public CmdMe(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.me")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.MUST_SPECIFY_MESSAGE);
            return true;
        }

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (PlayerConfig.getConfig(plugin, player).isMuted()) {
                Lang.send(sender, Lang.YOU_ARE_MUTED);
                return true;
            }
        }

        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', Lang.ME
                .replace("{sender}", sender.getName().equals("CONSOLE") ? "Console" : sender.getName())
                .replace("{message}", String.join(" ", Arrays.asList(args)))));

        if (plugin.getDiscordSRVHook() != null) {
            plugin.getDiscordSRVHook().sendToDiscord(Lang.ME_DISCORD
                    .replace("{sender}", sender.getName().equals("CONSOLE") ? "Console" : sender.getName())
                    .replace("{message}", String.join(" ", Arrays.asList(args))));
        }
        return true;
    }
}
