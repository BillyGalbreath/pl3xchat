package net.pl3x.bukkit.chat.commands;

import net.pl3x.bukkit.chat.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class CmdRussia implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.russia")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        String message = "ノ┬─┬ノ ︵ ( \\o°o)\\";
        if (args.length > 0) {
            message = String.join(" ", args) + " " + message;
        }

        ((Player) sender).chat(message);
        return true;
    }
}
