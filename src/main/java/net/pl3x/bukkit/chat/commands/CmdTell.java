package net.pl3x.bukkit.chat.commands;

import net.pl3x.bukkit.chat.Pl3xChat;
import net.pl3x.bukkit.chat.configuration.Lang;
import net.pl3x.bukkit.chat.configuration.PlayerConfig;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CmdTell implements TabExecutor {
    private final Pl3xChat plugin;

    public CmdTell(Pl3xChat plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        if (!sender.hasPermission("command.tell")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.MUST_SPECIFY_PLAYER);
            return true;
        }

        //noinspection deprecation
        Player target = Bukkit.getPlayer(args[0]);
        if (target == null) {
            Lang.send(sender, Lang.PLAYER_NOT_FOUND);
            return true;
        }

        if (args.length < 2) {
            Lang.send(sender, Lang.MUST_SPECIFY_MESSAGE);
            return true;
        }

        List<String> joined = new ArrayList<>(Arrays.asList(args));
        joined.remove(0);
        String message = String.join(" ", joined);
        if (Lang.isEmpty(message)) {
            Lang.send(sender, Lang.MUST_SPECIFY_MESSAGE);
            return true;
        }

        Player player = (Player) sender;
        PlayerConfig senderConfig = PlayerConfig.getConfig(plugin, player);
        if (senderConfig.isMuted()) {
            Lang.send(sender, Lang.YOU_ARE_MUTED);
            return true;
        }

        if (senderConfig.isIgnoring(target)) {
            Lang.send(sender, Lang.YOU_ARE_IGNORING
                    .replace("{target}", target.getName()));
            return true;
        }

        PlayerConfig targetConfig = PlayerConfig.getConfig(plugin, target);
        if (targetConfig.isIgnoring(player)) {
            Lang.send(sender, Lang.TARGET_IS_IGNORING
                    .replace("{target}", target.getName()));
            return true;
        }

        senderConfig.setReply(target);
        targetConfig.setReply(player);

        Lang.send(target, Lang.TELL_TARGET
                .replace("{sender}", sender.getName())
                .replace("{target}", target.getName())
                .replace("{message}", message));
        Lang.send(sender, Lang.TELL_SENDER
                .replace("{sender}", sender.getName())
                .replace("{target}", target.getName())
                .replace("{message}", message));
        Bukkit.getOnlinePlayers().stream()
                .filter(spy -> PlayerConfig.getConfig(plugin, spy).isSpying())
                .filter(spy -> spy != player && spy != target)
                .forEach(spy -> Lang.send(spy, Lang.SPY_PREFIX + Lang.TELL_SPY
                        .replace("{sender}", sender.getName())
                        .replace("{target}", target.getName())
                        .replace("{message}", message)));
        return true;
    }
}
