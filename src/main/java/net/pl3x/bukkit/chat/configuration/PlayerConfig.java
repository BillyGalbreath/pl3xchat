package net.pl3x.bukkit.chat.configuration;

import net.pl3x.bukkit.chat.Pl3xChat;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class PlayerConfig extends YamlConfiguration {
    private static final Map<Player, PlayerConfig> configs = new HashMap<>();

    public static PlayerConfig getConfig(Pl3xChat plugin, Player player) {
        synchronized (configs) {
            return configs.computeIfAbsent(player, k -> new PlayerConfig(plugin, player));
        }
    }

    public static void remove(Player player) {
        synchronized (configs) {
            configs.remove(player);
        }
    }

    public static void removeAll() {
        synchronized (configs) {
            configs.clear();
        }
    }

    private File file = null;
    private final Object saveLock = new Object();
    private final Player player;
    private Player reply;
    private final Pl3xChat plugin;

    private PlayerConfig(Pl3xChat plugin, Player player) {
        super();
        this.plugin = plugin;
        this.player = player;
        file = new File(plugin.getDataFolder(), "userdata" + File.separator + player.getUniqueId() + ".yml");
        reload();
    }

    private void reload() {
        synchronized (saveLock) {
            try {
                load(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    public Set<UUID> getIgnoreList() {
        return getStringList("ignore-list").stream()
                .map(UUID::fromString).collect(Collectors.toSet());
    }

    public void setIgnoreList(Set<UUID> ignoreList) {
        set("ignore-list", ignoreList.stream()
                .map(UUID::toString).collect(Collectors.toList()));
        save();
    }

    public boolean isIgnoring(Player target) {
        return !target.hasPermission("exempt.ignore") && // cannot ignore an exempt target
                getStringList("ignore-list").contains(target.getUniqueId().toString());
    }

    public boolean isMuted() {
        if (plugin.getAdvancedBanHook() != null) {
            // let AdvanceBan take over mutes
            return plugin.getAdvancedBanHook().isMuted(player);
        }
        return !player.hasPermission("exempt.mute") && // player is exempt from being muted
                getBoolean("muted", false);
    }

    public void setMuted(boolean isMuted) {
        assert !player.hasPermission("exempt.mute") : "Cannot mute an exempt player!";
        set("muted", isMuted);
        save();
    }

    public boolean isSpying() {
        return player.hasPermission("command.spy") && // not allowed to spy
                getBoolean("spy-mode", false);
    }

    public void setSpying(boolean isSpying) {
        assert !isSpying || !player.hasPermission("command.spy") : "Cannot enable spy on that player!";
        set("spy-mode", isSpying);
        save();
    }

    public void setReply(Player target) {
        reply = target;
    }

    public void removeReply(Player target) {
        if (reply == target) {
            reply = null;
        }
    }

    public Player getReply() {
        return reply;
    }
}
