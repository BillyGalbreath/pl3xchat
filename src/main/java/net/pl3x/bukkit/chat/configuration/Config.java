package net.pl3x.bukkit.chat.configuration;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class Config {
    public static boolean COLOR_LOGS;
    public static boolean DEBUG_MODE;
    public static String LANGUAGE_FILE;
    public static Map<String, String> TEXT_REPLACEMENTS = new HashMap<>();

    private Config() {
    }

    public static void reload(JavaPlugin plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");

        TEXT_REPLACEMENTS.clear();
        ConfigurationSection section = config.getConfigurationSection("text-replacements");
        for (String key : section.getKeys(false)) {
            TEXT_REPLACEMENTS.put(key, section.getString(key, ""));
        }
    }
}
