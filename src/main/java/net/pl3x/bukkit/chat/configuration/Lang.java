package net.pl3x.bukkit.chat.configuration;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION;
    public static String PLAYER_COMMAND;
    public static String PLAYER_NOT_FOUND;
    public static String MUST_SPECIFY_PLAYER;
    public static String MUST_SPECIFY_MESSAGE;

    public static String SAY;
    public static String SAY_DISCORD;
    public static String ME;
    public static String ME_DISCORD;

    public static String TARGET_IS_IGNORING;
    public static String YOU_ARE_IGNORING;
    public static String EXEMPT_IGNORE;
    public static String IGNORE_LIST;
    public static String TARGET_IGNORED;
    public static String TARGET_UNIGNORED;

    public static String TARGET_MUTED;
    public static String TARGET_UNMUTED;
    public static String EXEMPT_MUTE;
    public static String YOU_ARE_MUTED;

    public static String TELL_SENDER;
    public static String TELL_TARGET;
    public static String TELL_SPY;
    public static String REPLY_NO_TARGET;

    public static String SPY_MODE_TOGGLED;
    public static String SPY_MODE_TOGGLED_TARGET;
    public static String SPY_PREFIX;

    public static String CHAT_FORMAT;
    public static String STAFF_FORMAT;
    public static String DEATH_FORMAT;

    public static String HOVER_TOOLTIP;
    public static String CLICK_SUGGEST_COMMAND;

    public static String ITEM_FORMAT;
    public static String ITEM_FORMAT_MULTI;

    public static String ADVANCEMENT_DISCORD;

    public static String JOIN_FORMAT;
    public static String JOIN_GAME_DISCORD;
    public static String FIRST_JOIN_GAME_DISCORD;

    public static String QUIT_FORMAT;
    public static String QUIT_GAME_DISCORD;

    public static String VOTE_RECEIVED;

    public static String VERSION;
    public static String RELOAD;

    private Lang() {
    }

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        plugin.saveResource(Config.LANGUAGE_FILE, false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for this command!");
        PLAYER_COMMAND = config.getString("player-command", "&4Player only command!");
        PLAYER_NOT_FOUND = config.getString("player-not-found", "&4That player is not online!");
        MUST_SPECIFY_PLAYER = config.getString("must-specify-player", "&4Must specify a player!");
        MUST_SPECIFY_MESSAGE = config.getString("must-specify-message", "&4You must specify a message!");

        SAY = config.getString("broadcast", "&7[&6{sender}&7] &e{message}");
        SAY_DISCORD = config.getString("broadcast-discord", "*:loudspeaker: **{sender}**:*  {message}");
        ME = config.getString("action", "&6&o{sender} {message}");
        ME_DISCORD = config.getString("action-discord", "***{sender}*** *{message}*");

        TARGET_IS_IGNORING = config.getString("target-is-ignoring", "&7{target} &4is ignoring you!");
        YOU_ARE_IGNORING = config.getString("you-are-ignoring", "&4You are ignoring &7{target}&4!");
        EXEMPT_IGNORE = config.getString("exempt-ignore", "&7{target} &4cannot be ignored!");
        IGNORE_LIST = config.getString("ignore-list", "&dIgnore List:\n&7{list}");
        TARGET_IGNORED = config.getString("target-ignored", "&7{target} &dignored.");
        TARGET_UNIGNORED = config.getString("target-unignored", "&7{target} &dno longer ignored.");

        TARGET_MUTED = config.getString("target-muted", "&7{target} &dmuted.");
        TARGET_UNMUTED = config.getString("target-unmuted", "&7{target} &dunmuted.");
        EXEMPT_MUTE = config.getString("exempt-mute", "&7{target} &4 cannot be muted!");
        YOU_ARE_MUTED = config.getString("you-are-muted", "&4You are muted.");

        TELL_SENDER = config.getString("tell-sender", "&e[&7Me &d-> &7{target}&e] &7{message}");
        TELL_TARGET = config.getString("tell-target", "&e[&7Me &d<- &7{sender}&e] &7{message}");
        TELL_SPY = config.getString("tell-spy", "&e[&7{sender} &d-> &7{target}&e] &7{message}");
        REPLY_NO_TARGET = config.getString("reply-no-target", "&4No one to reply to!");

        SPY_MODE_TOGGLED = config.getString("spy-mode-toggled", "Spy mode toggled &7{toggle}&d.");
        SPY_MODE_TOGGLED_TARGET = config.getString("spy-mode-toggled-target", "Spy mode toggled &7{toggle} &dfor &7{target}&d.");
        SPY_PREFIX = config.getString("spy-prefix", "&e[&6Spy&e]");

        CHAT_FORMAT = config.getString("chat-format", "&r{prefix}{sender}{suffix}&e:&r {message}");
        STAFF_FORMAT = config.getString("staff-format", "&7[&4S&7]&r{prefix}{sender}{suffix}&e:&r {message}");
        DEATH_FORMAT = config.getString("death-format", "&6&o{message}");

        HOVER_TOOLTIP = config.getString("hover-tooltip", "{prefix}{group}&e:&r {name}");
        CLICK_SUGGEST_COMMAND = config.getString("click-suggest-command", "/msg {name} ");

        ITEM_FORMAT = config.getString("item-format", "&3[&e{item}&3]");
        ITEM_FORMAT_MULTI = config.getString("item-format-multi", "&3[&e{item} &7\u00D7&6{amount}&3]");

        ADVANCEMENT_DISCORD = config.getString("advancement-discord", ":medal: **{player} has made the advancement [{title}]**");

        JOIN_FORMAT = config.getString("join-format", "&a{message}");
        JOIN_GAME_DISCORD = config.getString("join-game-discord", ":plus: **{join-message}**");
        FIRST_JOIN_GAME_DISCORD = config.getString("first-join-game-discord", ":tada: **{join-message} for the first time**");

        QUIT_FORMAT = config.getString("quit-format", "&a{message}");
        QUIT_GAME_DISCORD = config.getString("quit-game-discord", ":minus: **{quit-message}**");

        VOTE_RECEIVED = config.getString("vote-received", ":blue_heart: **{player} has voted for us on {service} and was rewarded with a Rare Key!**");

        VERSION = config.getString("version", "&d{plugin} v{version}");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }

    public static boolean isEmpty(String message) {
        return message == null || message.isEmpty() || ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', message)).isEmpty();
    }
}
